import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../constants/app_assets.dart';
import '../constants/app_colors.dart';
import '../constants/app_routing.dart';
import '../generated/l10n.dart';

class AppNavBar extends StatelessWidget {
  const AppNavBar({
    Key? key,
    required this.current,
  }) : super(key: key);

  final int current;

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      currentIndex: current,
      selectedItemColor: AppColors.primary,
      unselectedItemColor: AppColors.neutral3,
      selectedFontSize: 14.0,
      unselectedFontSize: 14.0,
      items: [
        BottomNavigationBarItem(
          icon: SvgPicture.asset(
            AppAssets.svg.personList,
            color: current == 0 ? null : AppColors.neutral3,
          ),
          label: S.of(context).persons,
        ),
        BottomNavigationBarItem(
          icon: const Icon(Icons.settings_outlined),
          label: S.of(context).settings,
        ),
      ],
      onTap: (index) {
        if (index == 0) {
          Navigator.of(context).pushNamedAndRemoveUntil(
            AppRouting.personsListRoute,
            (route) => false,
          );
        } else if (index == 1) {
          Navigator.of(context).pushNamedAndRemoveUntil(
            AppRouting.settingsRoute,
            (route) => false,
          );
        }
      },
    );
  }
}
