class Person {
  const Person({
    this.id,
    this.name,
    this.statusCode,
    this.status,
    this.species,
    this.genderCode,
    this.gender,
    this.origin,
    this.location,
    this.image,
  });

  final String? genderCode;
  final String? gender;
  final int? id;
  final String? image;
  final String? location;
  final String? name;
  final String? origin;
  final String? species;
  final String? status;
  final String? statusCode;
}
