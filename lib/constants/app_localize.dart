import '../generated/l10n.dart';

class AppLocalize {
  static String getLocalize(context, String key) {
    switch (key) {
      case 'gender.Male':
        return S.of(context).male;
      case 'gender.Female':
        return S.of(context).female;
      case 'status.Alive':
        return S.of(context).alive;
      case 'status.Dead':
        return S.of(context).dead;
      default:
        return S.of(context).noData;
    }
  }
}