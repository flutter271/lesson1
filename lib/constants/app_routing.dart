class AppRouting {
  static const loginRoute = '/login';
  static const homeRoute = '/home';
  static const personsListRoute = '/personList';
  static const personsInfoRoute = '/personInfo';
  static const settingsRoute = '/settings';
}
