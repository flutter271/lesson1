abstract class AppAssets {
  static const images = _Images();
  static const svg = _Svg();
}

class _Images {
  const _Images();

  final String noAvatar = 'assets/images/bitmap/no_avatar.png';
  final String image01 = 'assets/images/bitmap/image_01.png';
  final String image02 = 'assets/images/bitmap/image_02.png';
  final String image03 = 'assets/images/bitmap/image_03.png';
  final String image04 = 'assets/images/bitmap/image_04.png';
  final String image05 = 'assets/images/bitmap/image_05.png';
  final String image06 = 'assets/images/bitmap/image_06.png';
  final String logo = 'assets/images/bitmap/logo.png';
  final String personTop = 'assets/images/bitmap/person_top.png';
  final String personBottom = 'assets/images/bitmap/person_bottom.png';
  final String background = 'assets/images/bitmap/background.png';

}

class _Svg {
  const _Svg();
  final String image01 = 'assets/images/svg/_image_01.svg';
  final String image02 = 'assets/images/svg/_image_02.svg';
  final String image03 = 'assets/images/svg/_image_03.svg';
  final String image04 = 'assets/images/svg/_image_04.svg';
  final String image05 = 'assets/images/svg/_image_05.svg';
  final String image06 = 'assets/images/svg/_image_06.svg';
  final String account = 'assets/images/svg/account.svg';
  final String password = 'assets/images/svg/password.svg';
  final String personList = 'assets/images/svg/person_list.svg';
}
