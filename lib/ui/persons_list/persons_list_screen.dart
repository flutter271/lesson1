import 'package:flutter/material.dart';
import 'package:lesson1_app/constants/app_routing.dart';
//import 'package:lesson1_app/ui/person_info/person_info.dart';

import '../../constants/app_assets.dart';
import '../../constants/app_colors.dart';
import '../../constants/app_styles.dart';
import '../../dto/person.dart';
import '../../generated/l10n.dart';
import '../../widgets/app_nav_bar.dart';
import 'widgets/person_grid_tile.dart';
import 'widgets/search_field.dart';
import 'widgets/person_list_tile.dart';
import 'dart:math';

part 'widgets/_list_view.dart';
part 'widgets/_grid_view.dart';

class PersonsList extends StatefulWidget {
  const PersonsList({Key? key}) : super(key: key);

  @override
  State<PersonsList> createState() => _PersonsListState();
}

class _PersonsListState extends State<PersonsList> {
  var isListView = true;
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        bottomNavigationBar: const AppNavBar(current: 0),
        body: Column(
          children: [
            const SearchField(),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 20.0),
              child: Row(
                children: [
                  Expanded(
                    child: Text(
                      S.of(context).personsTotal(_personsList.length).toUpperCase(),
                      style: AppStyles.s10w500.copyWith(
                        letterSpacing: 1.5,
                        color: AppColors.neutral2,
                      ),
                    ),
                  ),
                  IconButton(
                    icon: Icon(isListView ? Icons.list : Icons.grid_view),
                    iconSize: 28.0,
                    color: AppColors.neutral2,
                    onPressed: () {
                      setState(() {
                        isListView = !isListView;
                      });
                    },
                  ),
                ],
              ),
            ),
            Expanded(
              child: isListView
                  ? _ListView(personsList: _personsList)
                  : _GridView(personsList: _personsList),
            ),
          ],
        ),
      ),
    );
  }
}

final _personsList = generatePerson(10);

List<Person> generatePerson(int count){
  List<String> nameList = ['Рик', 'Алан', 'Саммер', 'Морти'];
  List<String> surNameList = ['Санчез', 'Райс', 'Смит'];
  List<String> raceList = ['Человек'];
  Map<String, String> statusList = {'Alive' : 'Живой', 'Dead' : 'Мертвый'};
  Map<String, String> genderList = {'Male': 'Мужской', 'Female' : 'Женский'};
  List<String> iconList = [AppAssets.images.image01,AppAssets.images.image02,AppAssets.images.image03,AppAssets.images.image04,AppAssets.images.image05,AppAssets.images.image06];
  List<Person> personsRndList = [];
  var rnd = Random();
    for (var i = 0; i < count; i++) {
    var genderCode = genderList.keys.elementAt(rnd.nextInt(genderList.length));
    var gender = genderList[genderCode];
    var name = nameList[rnd.nextInt(nameList.length)];
    var surName = surNameList[rnd.nextInt(surNameList.length)];
    var fullName = "$name $surName";
    var race = raceList[rnd.nextInt(raceList.length)];
    var statusCode = statusList.keys.elementAt(rnd.nextInt(statusList.length));
    var status = statusList[statusCode];
    var icon = iconList[rnd.nextInt(iconList.length)];
    Person p = Person(id:rnd.nextInt(1000000), name: fullName, statusCode: statusCode, status: status, species: race, genderCode: genderCode, gender: gender, image: icon );
    personsRndList.add(p);
  }
  return personsRndList;
}

