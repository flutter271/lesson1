import 'package:flutter/material.dart';

import '../../../constants/app_colors.dart';
import '../../../constants/app_localize.dart';
import '../../../constants/app_styles.dart';
import '../../../dto/person.dart';
import '../../../generated/l10n.dart';
import '../../../widgets/user_avatar.dart';

class PersonListTile extends StatelessWidget {
  const PersonListTile(this.person, {Key? key}) : super(key: key);

  final Person person;

  Color _statusColor(String? status) {
    if (status == 'Dead') return Colors.red;
    if (status == 'Alive') return const Color(0xff00c48c);
    return Colors.grey;
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        UserAvatar(
          person.image,
          margin: const EdgeInsets.only(right: 20.0),
        ),
        Expanded(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Row(
                children: [
                  Expanded(
                    child: Text(
                      AppLocalize.getLocalize(context, 'status.${person.statusCode}'),
                      style: AppStyles.s10w500.copyWith(
                        letterSpacing: 1.5,
                        color: _statusColor(
                          person.statusCode,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  Expanded(
                    child: Text(
                      person.name ?? S.of(context).noData,
                      style: AppStyles.s16w500.copyWith(
                        height: 1.6,
                        leadingDistribution: TextLeadingDistribution.even,
                      ),
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  Expanded(
                    child: Text(
                      //'${person.species ?? S.of(context).noData}, ${person.gender ?? S.of(context).noData}',
                      '${person.species ?? S.of(context).noData}, ${AppLocalize.getLocalize(context, 'gender.${person.genderCode}')}',
                      style: const TextStyle(
                        color: AppColors.neutral2,
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),
        )
      ],
    );
  }
}
