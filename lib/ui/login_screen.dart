import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:lesson1_app/constants/app_routing.dart';
import '../constants/app_assets.dart';
import '../constants/app_colors.dart';
import '../constants/app_styles.dart';
import '../generated/l10n.dart';
import 'home_screen.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final formKey = GlobalKey<FormState>();

  String? login;
  String? password;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.background,
      body: Column(
        children: [
          Expanded(
            child: Image.asset(
              AppAssets.images.logo,
            ),
          ),
          Expanded(
            child: Center(
              child: Form(
                key: formKey,
                child: Padding(
                  padding: const EdgeInsets.all(
                    16.0,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        S.of(context).inputLoginAndPassword,
                        style: const TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      TextFormField(
                        decoration: InputDecoration(
                          label: Text(S.of(context).login),
                          hintText: S.of(context).login,
                          hintStyle: AppStyles.s16w400.copyWith(
                            color: AppColors.neutral2,
                          ),
                          prefixIcon: Padding(
                            padding: const EdgeInsets.symmetric(
                              horizontal: 16.0,
                            ),
                            child: SvgPicture.asset(
                              AppAssets.svg.account,
                              width: 16.0,
                              color: AppColors.neutral2,
                            ),
                          ),
                        ),
                        maxLength: 8,
                        validator: (value) {
                          if (value == null) {
                            return S.of(context).inputErrorCheckLogin;
                          }
                          if (value.length < 3) {
                            return S.of(context).inputErrorLoginIsShort;
                          }
                          return null;
                        },
                        onSaved: (value) {
                          login = value;
                        },
                      ),
                      TextFormField(
                        decoration: InputDecoration(
                          label: Text(
                            S.of(context).password,
                          ),
                          hintText: S.of(context).password,
                          hintStyle: AppStyles.s16w400.copyWith(
                            color: AppColors.neutral2,
                          ),
                          prefixIcon: Padding(
                            padding: const EdgeInsets.symmetric(
                              horizontal: 16.0,
                            ),
                            child: SvgPicture.asset(
                              AppAssets.svg.password,
                              width: 16.0,
                              color: AppColors.neutral2,
                            ),
                          ),
                        ),
                        obscureText: true,
                        maxLength: 16,
                        validator: (value) {
                          if (value == null) {
                            return S.of(context).inputErrorCheckPassword;
                          }
                          if (value.length < 8) {
                            return S.of(context).inputErrorPasswordIsShort;
                          }
                          return null;
                        },
                        onSaved: (value) {
                          password = value;
                        },
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(
              16.0,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Expanded(
                  child: ElevatedButton(
                    child: Text(
                      S.of(context).signIn,
                    ),
                    onPressed: () {
                      final isValidated =
                          formKey.currentState?.validate() ?? false;
                      if (isValidated) {
                        FocusScope.of(context).unfocus();
                        formKey.currentState?.save();
                        if (login == 'qwerty' && password == '123456ab') {
                          Navigator.pushNamed(
                            context,
                            AppRouting.personsListRoute,
                          );
                        } else {
                          Container();
                          showDialog(
                            context: context,
                            builder: (context) => AlertDialog(
                              title: Text(
                                S.of(context).tryAgain,
                              ),
                              actions: [
                                ElevatedButton(
                                  onPressed: () {
                                    Navigator.pop(
                                      context,
                                    );
                                  },
                                  child: Text(
                                    S.of(context).close,
                                  ),
                                )
                              ],
                            ),
                          );
                        }
                      }
                    },
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
