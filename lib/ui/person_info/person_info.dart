import 'package:flutter/material.dart';
//import 'package:lesson1_app/dto/person.dart';

//import '../../dto/person.dart';

class PersonsInfo extends StatelessWidget {
  //const PersonsInfo(this.person, {Key? key}) : super(key: key);
  const PersonsInfo({Key? key}) : super(key: key);
  //final Person person;

  @override
  Widget build(BuildContext context) {
//    final arguments = (ModalRoute.of(context)?.settings.arguments ?? const Person());
    final arguments = (ModalRoute.of(context)?.settings.arguments ??  <String, dynamic>{}) as Map;
    debugPrint(arguments.toString());
    return Scaffold(
      appBar: AppBar( 
        title: const Text('TTTTTT'),
      ),
      body: Stack(
        textDirection: TextDirection.ltr,
        children: <Widget>[
          Container(
            width: 200,
            height: 200,
            color: Colors.blueGrey,
          ),
          Container(
            width: 160,
            height: 160,
            color: Colors.cyan,
          ),
          Container(
            width: 100,
            height: 100,
            color: Colors.blue,
          ),
        ],
      ),
    );
  }
}
