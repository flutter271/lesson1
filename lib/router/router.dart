import 'package:flutter/material.dart';
import 'package:lesson1_app/ui/home_screen.dart';
import 'package:lesson1_app/ui/login_screen.dart';
import 'package:lesson1_app/ui/person_info/person_info.dart';
import 'package:lesson1_app/ui/persons_list/persons_list_screen.dart';
import 'package:lesson1_app/ui/settings_screen.dart';

import '../constants/app_routing.dart';

class AppRouter {
  static Route generateRoute(RouteSettings routeSettings) {
    switch (routeSettings.name) {
      case AppRouting.homeRoute:
        return MaterialPageRoute(
          builder: (context) => const HomeScreen(),
        );
      case AppRouting.personsListRoute:
        return MaterialPageRoute(
          builder: (context) => const PersonsList(),
        );
      case AppRouting.personsInfoRoute:
        return MaterialPageRoute(
          builder: (context) => const PersonsInfo(),
        );
      case AppRouting.settingsRoute:
        return MaterialPageRoute(
          builder: (context) => const SettingsScreen(),
        );
      default:
        return MaterialPageRoute(
          builder: (context) => const LoginScreen(),
        );
    }
  }
}